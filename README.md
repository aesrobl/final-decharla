# ENTREGA CONVOCATORIA MAYO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Adrián Espinosa Robles
* Titulación: Doble grado GIST + ADE
* Cuenta en laboratorios: aesrobl
* Cuenta URJC: a.espinosar.2018
* Video básico (url): https://www.youtube.com/watch?v=pWCc7BEI4-E
* Video parte opcional (url): https://www.youtube.com/watch?v=kDbFih5JUC8
* Despliegue (url): http://aesrobl.pythonanywhere.com/
* Contraseñas: xx34d23, yy45e34
* Cuenta Admin Site: adriadmin/Md1dA,pf!

## Resumen parte obligatoria
Antes de comenzar, la app pedirá una contraseña de acceso al usuario para poder acceder a la página. Una vez ha accedido con una contraseña válida, no tendrá que volver a autentificarse gracias a una cookie de sesión. En todas las páginas se presenta la cabecera, el menú con los recursos disponibles, un formulario para acceder a una sala (creada o no) y un pie de página con estadísticas de las salas. En la página principal se muestra un listado de todas las salas creadas, sus mensajes y mensajes desde la última visita. En la página de cada sala se puede comentar un texto o imagen, que aparecerán debajo del formulario ordenados de más reciente a menos, así como un botón para acceder a la página en formato JSON. En la página de configuración se podrá cambiar el nombre del charlador, y el estilo y tamaño de letra para mostrar los párrafos y mensajes. En la página de ayuda se muestran detalles de la autoría de la práctica y una breve descripción de su funcionamiento. Adicionalmente, también existe un recurso de sala dinámica, donde los mensajes se actualizan gracias a un script de Javascript y un recurso donde se podrá realizar un PUT de un archivo XML para actualizar los mensajes de una sala (esta última funcionalidad no funciona en el despliegue por error 403, pero si en la ejecución en local con csrf-exempt).

## Lista partes opcionales

* Adición del favicon.ico: Se ha añadido a la app un favicon para mostrar en navegadores.
* Adición de un recurso logout: Se ha añadido un recurso logout que permite dar de baja al usuario que se ha logeado y redirigirle a la página de login de nuevo.
* Modificación del script Javascript: Se ha modificado el script para que muestre los mensajes de las salas dinámicas con formato similar al de las salas originales.
* Extensión de tests Django: Se han incluido varios tests extremo a extremo con más de una invocación de recurso, así como tests unitarios que prueban una parte del código respectivo al código de los modelos.
