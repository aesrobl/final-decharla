from django.contrib import admin


from .models import Room, Speaker, Message, AccessKeys
# Register your models here.

admin.site.register(Room)
admin.site.register(Speaker)
admin.site.register(Message)
admin.site.register(AccessKeys)
