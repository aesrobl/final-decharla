from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('configuracion', views.configuration, name='configuration'),
    path('ayuda', views.help, name='help'),
    path('login', views.login, name='login'),
    path('logout', views.logout_view, name='logout_view'),
    path('dinamica/<str:room_name>', views.dynamic_rooms),
    path('update/<str:room_name>', views.update_rooms),
    path('json/<str:room_name>', views.json_room),
    path('<str:room_name>', views.rooms, name='rooms'),
]
