from django.db import models


# Create your models here.


class Room(models.Model):
    name = models.CharField(max_length=50, verbose_name="Nombre de la sala")
    total_messages_text = models.IntegerField(default=0)
    total_messages_image = models.IntegerField(default=0)

    def get_total_messages(self):
        return self.total_messages_image + self.total_messages_text

    def is_empty(self):
        return self.get_total_messages() == 0

    def __str__(self):
        return "El nombre de la sala es: " + str(self.name) + " y tiene " + str(self.total_messages_text) + \
            " mensajes de texto y " + str(self.total_messages_image) + " mensajes de imagen"


class Speaker(models.Model):
    FONT_SIZE_CHOICES = (
        ('small', 'Pequeño'),
        ('medium', 'Mediano'),
        ('large', 'Grande'),
        ('x-large', 'Extra Grande'),
        ('xx-large', 'Muy Extra Grande'),
        ('xxx-large', 'Super Grande')
    )
    FONT_TYPE_CHOICES = (
        ('Arial', 'Arial'),
        ('Times New Roman', 'Times New Roman'),
        ('Verdana', 'Verdana'),
        ('Helvetica', 'Helvetica'),
        ('Georgia', 'Georgia'),
        ('Open Sans', 'Open Sans')
    )
    name = models.CharField(max_length=50, default='Anónimo', verbose_name="Nombre de usuario")
    cookie_id = models.IntegerField()
    last_connected = models.DateTimeField()
    font_size = models.CharField(max_length=10, choices=FONT_SIZE_CHOICES, default='medium',
                                 verbose_name="Tamaño de letra")
    font_type = models.CharField(max_length=30, choices=FONT_TYPE_CHOICES, default='Arial',
                                 verbose_name="Fuente de estilo de letra")

    def __str__(self):
        return "El charlador es: " + str(self.name) + " con Cookie_Id: " + str(self.cookie_id) + \
            " y conectado por última vez: " + str(self.last_connected)


class Message(models.Model):
    content = models.TextField(verbose_name="Contenido del mensaje")
    isimage = models.BooleanField(verbose_name="¿Es una imagen?")
    published = models.DateTimeField('date')
    speaker = models.ForeignKey(Speaker, on_delete=models.CASCADE)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)

    def __str__(self):
        if self.isimage:
            return "Autor: " + self.speaker.name + ", en sala " + self.room.name + " ha publicado la imagen: " \
                   + str(self.content)
        else:
            return "Autor: " + self.speaker.name + ", en sala " + self.room.name + " ha dicho: " + str(self.content)


class AccessKeys(models.Model):
    key = models.TextField(verbose_name="Contraseña de acceso")

    def __str__(self):
        return "Una de las claves válidas con identificador " + str(self.id) + " es: " + str(self.key)
