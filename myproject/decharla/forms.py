from django import forms

# Archivo de python para crear los formularios con Django

from .models import Room, Speaker, Message, AccessKeys


class RoomForm(forms.ModelForm):
    class Meta:  # Clase meta donde le indico de donde coge los datos y qué datos
        model = Room
        fields = ('name',)


class SpeakerForm(forms.ModelForm):
    class Meta:
        model = Speaker
        fields = ('name',)


class StyleForm(forms.ModelForm):
    class Meta:
        model = Speaker
        fields = ('font_size', 'font_type')


class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ('content', 'isimage')


class AuthenticateForm(forms.ModelForm):
    class Meta:
        model = AccessKeys
        fields = ('key', )
