from django.test import TestCase
from django.test import Client

from .models import AccessKeys, Room


# Create your tests here.

class ClassFunctions(TestCase):  # Tests unitarios que prueban funciones del modelo Room

    def test_get_total_messages(self):
        room = Room(name="prueba")  # Creamos una nueva sala desde cero
        self.assertEqual(room.get_total_messages(), 0)  # Al principio no debería tener mensajes ni de texto ni imagen
        room.total_messages_image = 9
        self.assertEqual(room.get_total_messages(), 9)  # Una vez añadidos 9 mensajes de imagen debería
        # tener un total de 9
        room.total_messages_text = 2
        self.assertEqual(room.get_total_messages(), 11)  # Una vez añadidos los de texto debería tener 11 en total

    def test_is_empty(self):
        room = Room(name="prueba")
        self.assertEqual(room.is_empty(), True)  # La acabamos de crear, debería estar vacía
        room.total_messages_image = 5
        self.assertEqual(room.is_empty(), False)


class ResourceTests(TestCase):  # Tests HTTP extremo a extremo que prueban la API
    def test_main(self):
        """Comprobamos que se realiza correctamente el GET al recurso principal"""
        c = Client()
        response = c.get('/')
        self.assertRedirects(response, '/login?redir=', status_code=302)  # Vemos que se redirija a la página correcta
        clave = AccessKeys.objects.create(key="prueba")  # Creamos una clave válida en la base de datos
        response = c.post('/login?redir=', {'key': clave.key})
        self.assertRedirects(response, '/', status_code=302)  # Comprobamos que redirija a la página principal
        response = c.get('/')  # Comprobamos ahora que directamente se responde con la página principal,
        # sin redirecciones por tener la cookie ya guardada en la base de datos
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h1>Listado de Salas</h1>', content)

    def test_configuration(self):
        """Comprobamos que se realiza correctamente el GET al recurso configuración"""
        c = Client()
        response = c.get('/configuracion')
        self.assertRedirects(response, '/login?redir=configuracion',
                             status_code=302)  # Vemos que se redirija a la página correcta
        clave = AccessKeys.objects.create(key="prueba")  # Creamos una clave válida en la base de datos
        response = c.post('/login?redir=configuracion', {'key': clave.key})
        self.assertRedirects(response, '/configuracion', status_code=302)  # Comprobamos que redirija a la página de
        # configuración
        response = c.get('/configuracion')  # Comprobamos ahora que directamente se responde con la página de
        # configuración, sin redirecciones por tener la cookie ya guardada en la base de datos
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h1>Configuración</h1>', content)

    def test_help(self):
        """Comprobamos que se realiza correctamente el GET al recurso ayuda"""
        c = Client()
        response = c.get('/ayuda')
        self.assertRedirects(response, '/login?redir=ayuda',
                             status_code=302)  # Vemos que se redirija a la página correcta
        clave = AccessKeys.objects.create(key="prueba")  # Creamos una clave válida en la base de datos
        response = c.post('/login?redir=ayuda', {'key': clave.key})
        self.assertRedirects(response, '/ayuda', status_code=302)  # Comprobamos que redirija a la página de
        # ayuda
        response = c.get('/ayuda')  # Comprobamos ahora que directamente se responde con la página de
        # ayuda, sin redirecciones por tener la cookie ya guardada en la base de datos
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h1>Ayuda</h1>', content)

    def test_room(self):
        """Comprobamos que se realiza correctamente el GET al recurso de una sala cualquiera"""
        c = Client()
        response = c.get('/chuli')
        self.assertRedirects(response, '/login?redir=chuli',
                             status_code=302)  # Vemos que se redirija a la página correcta
        clave = AccessKeys.objects.create(key="prueba")  # Creamos una clave válida en la base de datos
        response = c.post('/login?redir=chuli', {'key': clave.key})
        self.assertRedirects(response, '/chuli', status_code=302)  # Comprobamos que redirija a la página de
        # la sala que queríamos
        response = c.get('/chuli')  # Comprobamos ahora que directamente se responde con la página de
        # la página concreta, sin redirecciones por tener la cookie ya guardada en la base de datos
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h2>¡No hay ninguna conversación aún!</h2>', content)

    def test_login(self):
        """Comprobamos que se realiza correctamente el GET al login"""
        c = Client()
        response = c.get('/login')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h2>Autentícate</h2>', content)
        clave = AccessKeys.objects.create(key="prueba")  # Creamos una clave válida en la base de datos
        response = c.post('/login', {'key': clave.key})
        self.assertRedirects(response, '/', status_code=302)  # Comprobamos que redirija a la página principal

    def test_logout_view(self):
        """Comprobamos que se realiza correctamente el GET al logout, después de haber iniciado sesión"""
        c = Client()
        c.get('/login')
        clave = AccessKeys.objects.create(key="prueba")  # Creamos una clave válida en la base de datos
        c.post('/login', {'key': clave.key})  # Hacemos que el cliente se autentifique
        # Aquí empieza el test real de logout
        response = c.get('/logout')
        self.assertRedirects(response, '/login', status_code=302)  # Comprobamos que redirija a la página de login

    def test_json_room(self):
        """Comprobamos que se realiza correctamente el GET a una sala en formato JSON"""
        c = Client()
        response = c.get('/json/chuli')
        self.assertRedirects(response, '/login?redir=json/chuli', status_code=302)
        clave = AccessKeys.objects.create(key="prueba")
        response = c.post('/login?redir=json/chuli', {'key': clave.key})
        self.assertRedirects(response, '/json/chuli', status_code=302)  # Comprobamos que redirija a la página de
        # la sala que queríamos
        response = c.get('/json/chuli')  # Comprobamos ahora que directamente se responde con la página de
        # la página json concreta, sin redirecciones por tener la cookie ya guardada en la base de datos
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('[]', content)

    def test_dynamic(self):
        """Comprobamos que se realiza correctamente el GET a la página dinámica de cada sala"""
        c = Client()
        response = c.get('/dinamica/chuli')
        self.assertRedirects(response, '/login?redir=dinamica/chuli', status_code=302)
        clave = AccessKeys.objects.create(key="prueba")  # Creamos una clave válida en la base de datos
        response = c.post('/login?redir=dinamica/chuli', {'key': clave.key})
        self.assertRedirects(response, '/dinamica/chuli', status_code=302)  # Comprobamos que redirija a la página
        # dinámica de la sala creada
        response = c.get('/dinamica/chuli')  # Comprobamos ahora que directamente se responde con la página de
        # la página dinámica concreta, sin redirecciones por tener la cookie ya guardada en la base de datos
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h2>Listado de mensajes</h2>', content)

    def test_update_rooms(self):
        """Comprobamos que se realiza correctamente el camino de PUT al recurso para actualizar salas"""
        c = Client()
        clave = AccessKeys.objects.create(key="prueba")  # Creamos una clave válida en la base de datos
        response = c.put('/update/chuli')  # Hacemos el PUT sin la cabecera Authorization
        self.assertEqual(response.status_code, 401)  # Vemos que salta un código de no autorización por no poner
        # la cabecera
        # header = {'HTTP_AUTHORIZATION': clave.key}
        # response = c.put('/update/chuli', header=header)
        # self.assertRedirects(response, '/chuli', status_code=302)  # NO vemos que redirije a la página de sala donde
        # se han actualizado los mensajes
