from xml.sax.handler import ContentHandler
from .models import Message

from django.utils import timezone


class RoomUpdateHandler(ContentHandler):

    def __init__(self, room, speaker):
        self.room = room
        self.speaker = speaker
        self.current_message = None  # Será los diccionarios donde iremos guardando la información de cada mensaje
        self.messages = []  # Lista de la información de todos los mensajes que van en el documento
        self.inContent = False
        self.theContent = ""

    def startElement(self, name, attrs):
        if name == 'message':
            self.current_message = {'isimg': attrs.get('isimg')}
        elif name == 'text':
            self.inContent = True

    def endElement(self, name):
        if name == "message":  # Si ya estamos en el final de un mensaje
            self.messages.append(self.current_message)  # Guardamos la info del mensaje (dict) en la lista de mensajes
            self.current_message = None  # Volvemos a dejar vacío el campo para guardar información
        elif name == 'text':  # Si ya estamos en el final del cuerpo del mensaje
            self.current_message['text'] = self.theContent
        if self.inContent:  # Vaciamos el content para el siguiente texto de un mensaje
            self.inContent = False
            self.theContent = ""

    def characters(self, chars):  # Si me encuentro con los caractéres dentro del texto, el manejador vendrá aquí
        if self.inContent:
            self.theContent = self.theContent + chars

    def update_room(self):
        for message in self.messages:
            is_image = message.get('isimg') == "true"  # Guardamos el booleano para poder guardar el mensaje
            content = message.get('text')
            message = Message(content=content, isimage=is_image, published=timezone.now(), speaker=self.speaker,
                              room=self.room)
            message.save()
        # Actualizamos los valores de mensajes totales
        self.room.total_messages_text = self.room.message_set.filter(isimage=False).count()
        self.room.total_messages_image = self.room.message_set.filter(isimage=True).count()
        self.room.save()
